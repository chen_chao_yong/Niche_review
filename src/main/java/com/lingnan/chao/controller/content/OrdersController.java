package com.lingnan.chao.controller.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lingnan.chao.dto.CommentDto;
import com.lingnan.chao.dto.OrdersDto;
import com.lingnan.chao.service.OrdersService;

@Controller
@RequestMapping("/orders")
public class OrdersController {
	
	@Autowired
	private OrdersService ordersService;
	
	@RequestMapping
	public String init(Model model, OrdersDto dto) {
		model.addAttribute("list", ordersService.searchByPage(dto));
		model.addAttribute("searchParam", dto);
		return "/content/orderList";
	}
}
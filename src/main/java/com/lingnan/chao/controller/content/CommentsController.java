package com.lingnan.chao.controller.content;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lingnan.chao.dto.CommentDto;
import com.lingnan.chao.service.CommentService;

@Controller
@RequestMapping("/comments")
public class CommentsController {
	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping
	public String init(Model model, CommentDto dto) {
		model.addAttribute("list", commentService.searchByPage(dto));
		model.addAttribute("searchParam", dto);
		return "/content/commentList";
	}
}

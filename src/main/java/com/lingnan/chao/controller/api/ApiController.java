package com.lingnan.chao.controller.api;

import java.util.List;

import javax.annotation.Resource;

import com.lingnan.chao.bean.Page;
import com.lingnan.chao.constant.ApiCodeEnum;
import com.lingnan.chao.dto.AdDto;
import com.lingnan.chao.dto.ApiCodeDto;
import com.lingnan.chao.dto.BusinessDto;
import com.lingnan.chao.dto.BusinessListDto;
import com.lingnan.chao.dto.CommentForSubmitDto;
import com.lingnan.chao.dto.CommentListDto;
import com.lingnan.chao.dto.OrderForBuyDto;
import com.lingnan.chao.dto.OrdersDto;
import com.lingnan.chao.service.AdService;
import com.lingnan.chao.service.BusinessService;
import com.lingnan.chao.service.CommentService;
import com.lingnan.chao.service.MemberService;
import com.lingnan.chao.service.OrdersService;
import com.lingnan.chao.util.CommonUtil;
import com.lingnan.chao.util.MD5Util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private AdService adService;

	@Resource
	private BusinessService businessService;

	@Resource
	private MemberService memberService;

	@Resource
	private OrdersService ordersService;
	
	@Resource
	private CommentService commentService;

	@Value("${ad.number}")
	private int adNumber;

	@Value("${businessHome.number}")
	private int businessHomeNumber;

	@Value("${businessSearch.number}")
	private int businessSearchNumber;

	/**
	 * 首页 —— 广告（超值特惠）
	 */
	@CrossOrigin
	@RequestMapping(value = "/homead", method = RequestMethod.GET)
	public List<AdDto> homead() {
		AdDto adDto = new AdDto();
		adDto.getPage().setPageNumber(adNumber);
		return adService.searchByPage(adDto);
	}

	/**
	 * 首页 —— 推荐列表（猜你喜欢）/homelist/{city}/{page.currentPage}
	 */
	@CrossOrigin
	@RequestMapping(value = "/homelist", method = RequestMethod.GET)
	public BusinessListDto homelist(BusinessDto businessDto) {
		businessDto.getPage().setPageNumber(businessHomeNumber);
		return businessService.searchByPageForApi(businessDto);
	}

	/**
	 * 搜索结果页 - 搜索结果 - 三个参数/search/{page.currentPage}/{city}/{category}/{keyword}
	 */
	@CrossOrigin
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public BusinessListDto searchByKeyword(BusinessDto businessDto) {
		businessDto.getPage().setPageNumber(businessSearchNumber);
		return businessService.searchByPageForApi(businessDto);
	}

	/**
	 * 搜索结果页 - 搜索结果 - 两个参数
	 */
	@CrossOrigin
	@RequestMapping(value = "/search/{page.currentPage}/{city}/{category}", method = RequestMethod.GET)
	public BusinessListDto search(BusinessDto businessDto) {
		businessDto.getPage().setPageNumber(businessSearchNumber);
		return businessService.searchByPageForApi(businessDto);
	}

	/**
	 * 详情页 - 商户信息
	 */
	@CrossOrigin
	@RequestMapping(value = "/detail/info/{id}", method = RequestMethod.GET)
	public BusinessDto detail(@PathVariable("id") Long id) {
		return businessService.getById(id);
	}

	/**
	 * 详情页 - 用户评论/detail/comment/{currentPage}/{businessId}
	 */
	@CrossOrigin
	@RequestMapping(value = "/detail/comment", method = RequestMethod.GET)
	public CommentListDto getComment(@RequestParam("businessId") Long businessId) {
		return commentService.getListByBusinessId(businessId);
	}

	/**
	 * 订单列表
	 */
	@CrossOrigin
	@RequestMapping(value = "/orderlist", method = RequestMethod.GET)
	public List<OrdersDto> orderlist(@RequestParam("username") Long username) {
		System.out.println(username);
		// 根据手机号取出会员ID
		Long memberId = memberService.getIdByPhone(username);
		return ordersService.getListByMemberId(memberId);
	}

	/**
	 * 提交评论
	 */
	@CrossOrigin
	@RequestMapping(value = "/submitComment", method = RequestMethod.POST)
	public ApiCodeDto submitComment(@RequestBody CommentForSubmitDto dto) {
		ApiCodeDto result;
		// TODO 需要完成的步骤：
		// 1、校验登录信息：token、手机号
		Long phone = memberService.getPhone(dto.getToken());
		if (phone != null && phone.equals(dto.getUsername())) {
			// 2、根据手机号取出会员ID
			Long memberId = memberService.getIdByPhone(phone);
			// 3、根据提交上来的订单ID获取对应的会员ID，校验与当前登录的会员是否一致
			OrdersDto ordersDto = ordersService.getById(dto.getId());
			if(ordersDto.getMemberId().equals(memberId)) {
				// 4、保存评论
				System.out.println(commentService.add(dto));
				result = new ApiCodeDto(ApiCodeEnum.SUCCESS);
				// TODO
				// 5、还有一件重要的事未做
			} else {
				result = new ApiCodeDto(ApiCodeEnum.NO_AUTH);
			}
		} else {
			result = new ApiCodeDto(ApiCodeEnum.NOT_LOGGED);
		}
		return result;
	}

	/**
	 * 根据手机号下发短信验证码
	 */
	@CrossOrigin
	@RequestMapping(value = "/sms", method = RequestMethod.POST)
	public ApiCodeDto sms(@RequestParam("username") Long username) {
		ApiCodeDto dto;
		// 1、验证用户手机号是否存在（是否注册过）
		if (memberService.exists(username)) {
			// 2、生成6位随机数
			String code = String.valueOf(CommonUtil.random(6));
			// 3、保存手机号与对应的md5(6位随机数)（一般保存1分钟，1分钟后失效）
			if (memberService.saveCode(username, code)) {
				// 4、调用短信通道，将明文6位随机数发送到对应的手机上。
				if (memberService.sendCode(username, code)) {
					dto = new ApiCodeDto(ApiCodeEnum.SUCCESS.getErrno(), code);
				} else {
					dto = new ApiCodeDto(ApiCodeEnum.SEND_FAIL);
				}
			} else {
				dto = new ApiCodeDto(ApiCodeEnum.REPEAT_REQUEST);
			}
		} else {
			dto = new ApiCodeDto(ApiCodeEnum.USER_NOT_EXISTS);
		}
		return dto;
	}

	/**
	 * 会员登录
	 */
	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ApiCodeDto login(@RequestParam("username") Long username, @RequestParam("code") String code) {
		ApiCodeDto dto;
		// 1、用手机号取出保存的md5(6位随机数)，能取到，并且与提交上来的code值相同为校验通过
		String saveCode = memberService.getCode(username);
		if (saveCode != null) {
			if (saveCode.equals(code)) {
				// 2、如果校验通过，生成一个32位的token
				String token = CommonUtil.getUUID();
				// 3、保存手机号与对应的token（一般这个手机号中途没有与服务端交互的情况下，保持10分钟）
				memberService.saveToken(token, username);
				// 4、将这个token返回给前端
				dto = new ApiCodeDto(ApiCodeEnum.SUCCESS);
				dto.setToken(token);
			} else {
				dto = new ApiCodeDto(ApiCodeEnum.CODE_ERROR);
			}
		} else {
			dto = new ApiCodeDto(ApiCodeEnum.CODE_INVALID);
		}
		return dto;
	}

	/**
	 * 买单
	 */
	@CrossOrigin
	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ApiCodeDto order(@RequestBody OrderForBuyDto orderForBuyDto) {
		ApiCodeDto dto;
		// 1、校验token是否有效（缓存中是否存在这样一个token，并且对应存放的会员信息（这里指的是手机号）与提交上来的信息一致）
		Long phone = memberService.getPhone(orderForBuyDto.getToken());
		if (phone != null && phone.equals(orderForBuyDto.getUsername())) {
			// 2、根据手机号获取会员主键
			Long memberId = memberService.getIdByPhone(phone);
			// 3、保存订单
			OrdersDto ordersDto = new OrdersDto();
			ordersDto.setNum(orderForBuyDto.getNum());
			ordersDto.setPrice(orderForBuyDto.getPrice());
			ordersDto.setBusinessId(orderForBuyDto.getId());
			ordersDto.setMemberId(memberId);
			ordersService.add(ordersDto);
			dto = new ApiCodeDto(ApiCodeEnum.SUCCESS);
			// 4、TODO 还有一件重要的事未做
			businessService.updateNumber(orderForBuyDto.getNum());
		} else {
			dto = new ApiCodeDto(ApiCodeEnum.NOT_LOGGED);
		}
		return dto;
	}
}

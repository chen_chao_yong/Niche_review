package com.lingnan.chao.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.lingnan.chao.bean.Comment;
import com.lingnan.chao.bean.Orders;
import com.lingnan.chao.constant.CommentStateConst;
import com.lingnan.chao.dao.OrdersDao;
import com.lingnan.chao.dto.CommentDto;
import com.lingnan.chao.dto.OrdersDto;
import com.lingnan.chao.service.OrdersService;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OrdersServiceImpl implements OrdersService {

	@Resource
	private OrdersDao ordersDao;
	
	@Value("${businessImage.url}")
    private String businessImageUrl;

	@Override
	public boolean add(OrdersDto ordersDto) {
		Orders orders = new Orders();
		BeanUtils.copyProperties(ordersDto, orders);
		orders.setCommentState(CommentStateConst.NOT_COMMENT);
		ordersDao.insert(orders);
		return true;
	}

	@Override
	public OrdersDto getById(Long id) {
		OrdersDto result = new OrdersDto();
		Orders orders = ordersDao.selectById(id);
		BeanUtils.copyProperties(orders, result);
		return result;
	}

	@Override
	public List<OrdersDto> getListByMemberId(Long memberId) {
		List<OrdersDto> result = new ArrayList<OrdersDto>();
		Orders ordersForSelect = new Orders();
		ordersForSelect.setMemberId(memberId);
		List<Orders>  ordersList = ordersDao.select(ordersForSelect);
		for(Orders orders : ordersList) {
			OrdersDto ordersDto = new OrdersDto();
			result.add(ordersDto);
			BeanUtils.copyProperties(orders, ordersDto);
			ordersDto.setImg(businessImageUrl + orders.getBusiness().getImgFileName());
			ordersDto.setTitle(orders.getBusiness().getTitle());
			ordersDto.setCount(orders.getNum());
		}
		return result;
	}

	@Override
	public List<OrdersDto> searchByPage(OrdersDto ordersDto) {
		List<OrdersDto> result = new ArrayList<>();
		Orders ordersForSelect = new Orders();
		BeanUtils.copyProperties(ordersDto, ordersForSelect);
		List<Orders> list = ordersDao.selectAll(ordersForSelect);
		for (Orders orders : list) {
			OrdersDto ordersDtoTemp = new OrdersDto();
			result.add(ordersDtoTemp);
			BeanUtils.copyProperties(orders, ordersDtoTemp);
		}
		return result;
	}
}
package com.lingnan.chao.service.impl;

import java.util.List;
import javax.annotation.Resource;
import com.lingnan.chao.bean.Dic;
import com.lingnan.chao.dao.DicDao;
import com.lingnan.chao.service.DicService;
import org.springframework.stereotype.Service;

@Service
public class DicServiceImpl implements DicService {
    
    @Resource
    private DicDao dicDao;
    
    @Override
    public List<Dic> getListByType(String type) {
	Dic dic = new Dic();
	dic.setType(type);
	return dicDao.select(dic);
    }
}

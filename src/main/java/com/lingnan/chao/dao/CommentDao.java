package com.lingnan.chao.dao;

import java.util.List;

import com.lingnan.chao.bean.Comment;

public interface CommentDao {
	/**
     *  根据查询条件分页查询评论列表
     * @param comment 查询条件
     * @return 评论列表
     */
    List<Comment> selectByPage(Comment comment);
    
    List<Comment> selectAll(Comment comment);
    
    /**
     * 新增
     * @param comment 评论对象
     * @return 影响行数
     */
    int insert(Comment comment);

	List<Comment> selectCommentList(Comment comment);
}
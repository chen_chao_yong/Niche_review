package com.lingnan.chao.dao;

import java.util.List;

import com.lingnan.chao.bean.Dic;

public interface DicDao {
    List<Dic> select(Dic dic);
}